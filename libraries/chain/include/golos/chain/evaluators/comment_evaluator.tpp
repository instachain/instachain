namespace golos {
    namespace chain {
        template
        class comment_evaluator<0, 16, 0>;

        template
        class comment_evaluator<0, 17, 0>;

        template
        class comment_options_evaluator<0, 16, 0>;

        template
        class comment_options_evaluator<0, 17, 0>;

        template
        class delete_comment_evaluator<0, 16, 0>;

        template
        class delete_comment_evaluator<0, 17, 0>;
    }
}